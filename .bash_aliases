# Refresh bash config
alias bashrefresh='source ~/.bashrc'

# Long list the files in directory
alias ll='ls -alF'

# List files with color
alias ls='ls --color=auto'

# Search files with color
alias grep='grep --color=auto'

# Use alert to be notified when long-running commands end, usage: sleep 1; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"' 

# Get your ip address
alias myip="curl http://ipecho.net/plain; echo"

# Get a log of timestamped git commits, usage: cd ~/mygitrepo; gitlog
alias gitlog='git log --date=local --pretty=format:"%h%x09%an%x09%ad%x09%s" | grep Jake > ~/Desktop/commits.txt'

# Initialize python projects to use virtual environment, linting, formatting, and type checking
alias pyinit='python -m venv .env && source .env/bin/activate && pip install --upgrade pip && pip install black && pip install mypy && cp -r ~/projects/scripts/.vscode-python ./.vscode'
