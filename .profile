# Source .bashrc if exists
if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# Set path to include user binaries 
PATH="$HOME/bin:$HOME/.local/bin:$PATH"

# Set bash colors
eval $(dircolors ~/.dircolors)
