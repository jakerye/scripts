# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Set title to user@host:dir with color
PS1='\[\e]0;\u@\h: \w\a\]\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\] '

# Set aliases
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Set anaconda path
export PATH="/home/jake/anaconda3/bin:$PATH"

# Set android paths
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# Set google cloud platform path
if [ -f '/home/jake/google-cloud-sdk/path.bash.inc' ]; then . '/home/jake/google-cloud-sdk/path.bash.inc'; fi

# Enable shell completion for google cloud platform
if [ -f '/home/jake/google-cloud-sdk/completion.bash.inc' ]; then . '/home/jake/google-cloud-sdk/completion.bash.inc'; fi

# Set node environment
export NODE_ENV=development

# Set node version manager directory
export NVM_DIR="$HOME/.nvm"

# Load node version manager
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# Enable shell completion for node version manager
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
